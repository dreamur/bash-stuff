#!/bin/bash


function data_query
{
    echo
    echo
    echo "Currently Installed BINARIES"
    ls -l /usr/local/bin | awk '{print $9}'

    echo
    echo
    echo "Currently Installed LIBRARIES"
    ls -l /usr/local/lib | awk '{print $9}'

    echo
    echo
    echo "Currently Installed INCLUDE DIRECTIVES"
    ls -l /usr/local/include | awk '{print $9}'

    echo
    echo
    echo "LINKER CACHE"
    echo
    cat /etc/ld.so.conf.d/libc.conf
}


clear

if [ $# -eq 3 ]
then
    INCLUDEDIR=$1
    LIBDIR=$2
    LIBNAME=$3

    echo "INSTALLING INCLUDE & LIB FILES"

    if [ $INCLUDEDIR ]
    then
      cp -r --preserve $INCLUDEDIR /usr/local/include/
      chown root:root "/usr/local/include/$LIBNAME/"
      chmod +r -R "/usr/local/include/$LIBNAME/"
      chmod +x "/usr/local/include/$LIBNAME/"
    fi
    if [ $LIBDIR ]
    then
      cp -r --preserve $LIBDIR /usr/local/lib/
      chown root:root "/usr/local/include/$LIBNAME/"
      chmod +r -R "/usr/local/include/$LIBNAME/"
      chmod +x "/usr/local/include/$LIBNAME/"
      cat "/usr/local/lib/$LIBNAME" >> /etc/ld.so.conf.d/libc.conf
    fi

    data_query

else
  if [ $# -gt 0 ]
  then
      echo "~Library Installation~"
      echo "sudo helper_script.sh <includeFiles> <includeFiles> <libName>"
      echo
      echo "~Data Query~"
      echo "helper_script.sh"
      echo
      echo
      echo "~Example~"
      echo "sudo helper_script /home/arsenik/Desktop/sfml-master/include/SFML/ /home/arsenik/Desktop/sfml-master/lib/SFML/ SFML"
  else
      data_query
  fi
fi
