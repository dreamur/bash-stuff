#!/bin/bash

# for more reading - for later - eventually
# https://stackoverflow.com/questions/6366530/bash-syntax-error-unexpected-end-of-file
# https://askubuntu.com/questions/66914/how-to-change-desktop-background-from-command-line-in-unity
# https://www.linuxjournal.com/content/bash-arrays
# https://www.cyberciti.biz/faq/bash-loop-over-file/
# https://stackoverflow.com/questions/1951506/add-a-new-element-to-an-array-without-specifying-the-index-in-bash
# http://www.tldp.org/LDP/abs/html/randomvar.html

declare -a FILE_LIST

if [ $# -gt 0 ]
then
    for filename in $*
    do
        FILE_LIST=("${FILE_LIST[@]}" $filename)
        #echo $filename
    done

    NUM_FILES=${#FILE_LIST[*]}
    index=$(($RANDOM%$NUM_FILES))
    gsettings set org.gnome.desktop.background picture-uri ${FILE_LIST[index]}

else
    echo "wallpaper_rotate.sh <source_img0> ... <source_imgN-1> <source_imgN>"
    echo
    echo "Give the script a path to a directory [or multiple directories] where your wallpapers are stored"
    echo
    echo "Example Usage"
    echo "  ./wallpaper_rotate.sh ~/Desktop/wallpapers/*"
fi
